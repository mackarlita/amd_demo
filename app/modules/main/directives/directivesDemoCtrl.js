(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:DirectivesDemoCtrl
	 * @description
	 * # DirectivesDemoCtrl
	 * Controller of the app
	 */

	angular
		.module('angular-material-demo')
		.controller('DirectivesDemoCtrl', DirectivesDemoCtrl);

	DirectivesDemoCtrl.$inject = ['$mdMedia', '$scope', '$mdDialog', '$mdToast'];

	function DirectivesDemoCtrl($mdMedia, $scope, $mdDialog, $mdToast) {
		var vm = this;

		vm.showToast = function(msg){
			$mdToast.show(
				$mdToast.simple()
					.textContent(msg)
					.position('top right'));
		};

		vm.enableFields = true;
		vm.areas = [
			{ id:1, name: 'Software Engineer' },
			{ id:2, name: 'Quality Assurance' },
			{ id:3, name: 'Help Desk' },
			{ id:4, name: 'Sales' }
		];
		vm.phones = [
			{ type: 'Home', number: '(555) 251-1234' },
			{ type: 'Cell', number: '(555) 786-9841' },
			{ type: 'Office', number: '(555) 314-1592' },
			{ type: 'Cell', number: '(555) 786-9841' },
			{ type: 'Office', number: '(555) 314-1592'},
			{ type: 'Home', number: '(555) 251-1234' },
			{ type: 'Cell', number: '(555) 786-9841' },
			{ type: 'Home', number: '(555) 251-1234' },
			{ type: 'Office', number: '(555) 314-1592' }
		];

		vm.tooglePlusOne = function(){
			vm.like = !vm.like;
		};

		vm.showNotificacion = function(){
			vm.showToast('Comment post simulation');
		};

		vm.shareModal = function(ev){
			var confirm = $mdDialog.confirm()
				.textContent('Would you like to share the content with your friends?')
				.targetEvent(ev)
				.ok('Please do it!')
				.cancel('Cancel');

			$mdDialog.show(confirm)
				.then(function() {
					vm.showToast('You shared this content');
				});

		};



		$scope.$watch(function(){
			return $mdMedia('lg') || $mdMedia('gt-lg');
		}, function(big) {
			vm.bigScreen = big;
		});

		$scope.$watch(function() {
			return $mdMedia('sm');
		}, function(big) {
			vm.isMobile = big;
		});

	}

})();
