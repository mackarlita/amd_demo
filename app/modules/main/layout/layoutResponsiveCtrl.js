(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:LayoutResponsiveCtrl
	 * @description
	 * # LayoutResponsiveCtrl
	 * Controller of the app
	 */

	angular
		.module('angular-material-demo')
		.controller('LayoutResponsiveCtrl', LayoutResponsiveCtrl);

	LayoutResponsiveCtrl.$inject = ['$mdMedia', '$scope'];

	function LayoutResponsiveCtrl($mdMedia, $scope) {
		var vm = this;

		$scope.$watch(function(){
				return $mdMedia('lg') || $mdMedia('gt-lg');
			}, function(big) {
				vm.bigScreen = big;
			});

		$scope.$watch(function() {
				return $mdMedia('sm');
			}, function(big) {
				vm.isMobile = big;
			});

	}

})();
