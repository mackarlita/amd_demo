'use strict';

	/**
	* @ngdoc function
	* @name app.route:HomeRoute
	* @description
	* # HomeRoute
	* Route of the app
	*/

angular.module('angular-material-demo')
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider

			.state('main', {
				url: '',
				abstract: true,
				templateUrl: 'app/modules/main/main.html',
				controller: 'MainCtrl',
				controllerAs: 'vm'
			})
			.state('main.layout', {
				url:'/layout',
				templateUrl: 'app/modules/main/layout/layout.html'
			})
			.state('main.children', {
				url:'/responsive',
				templateUrl: 'app/modules/main/layout/layoutResponsive.html',
				controller: 'LayoutResponsiveCtrl',
				controllerAs: 'vm'
			})
			.state('main.directives', {
				url:'/directives',
				templateUrl: 'app/modules/main/directives/directivesDemo.html',
				controller: 'DirectivesDemoCtrl',
				controllerAs: 'vm'
			});

	}]);
