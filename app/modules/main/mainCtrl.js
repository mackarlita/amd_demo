(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:MainCtrl
	 * @description
	 * # MainCtrl
	 * Controller of the app
	 */

	angular
		.module('angular-material-demo')
		.controller('MainCtrl', Main);

	Main.$inject = ['$mdSidenav'];

	function Main($mdSidenav) {
		var vm = this;

		vm.username = 'By Karla Martín';
		vm.title = 'Getting Started with Angular Material Design';

		vm.toggleLeftMenu = function(){
			$mdSidenav('uno-sidebar').toggle();
		};

	}

})();
