(function () {
	'use strict';

	/**
	 * @ngdoc configuration file
	 * @name app.config:config
	 * @description
	 * # Config and run block
	 * Configutation of the app
	 */


	angular
		.module('angular-material-demo')
		.config(configure)
		.run(runBlock);

	configure.$inject = ['$urlRouterProvider', '$httpProvider', '$mdThemingProvider'];

	function configure($urlRouterProvider, $httpProvider, $mdThemingProvider) {

		// This is required for Browser Sync to work poperly
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

		// New Color Palettes defined
		var unoSquareBlue = {
			'50': '#69e1f5',
			'100': '#52dcf3',
			'200': '#3ad7f2',
			'300': '#22d3f0',
			'400': '#10cae9',
			'500': 'rgb(14, 181, 209)',
			'600': '#0ca0b9',
			'700': '#0b8ca1',
			'800': '#097789',
			'900': '#086271',
			'A100': '#81e6f7',
			'A200': '#99ebf8',
			'A400': '#b1effa',
			'A700': '#064d59'
		};
		var unoSquareGrey = {
			'50': '#404040',
			'100': '#333333',
			'200': '#262626',
			'300': '#1a1a1a',
			'400': '#0d0d0d',
			'500': 'rgba(0, 0, 0, 0.7)',
			'600': '#000000',
			'700': '#000000',
			'800': '#000000',
			'900': '#000000',
			'A100': '#4d4d4d',
			'A200': '#595959',
			'A400': '#666666',
			'A700': '#000000'
		};

		// Adding color palettes to default theme
		$mdThemingProvider.definePalette('unoSquareBlue', unoSquareBlue);
		$mdThemingProvider.definePalette('unoSquareGrey', unoSquareGrey);

		// Definition of Color Palette for the Color Intentions of each theme
		$mdThemingProvider.theme('default')
			.primaryPalette('unoSquareBlue')
			.accentPalette('pink')
			.warnPalette('orange');

		$mdThemingProvider.theme('sideBarTheme')
			.primaryPalette('unoSquareGrey');

		$urlRouterProvider
			.otherwise('/layout');

	}

	runBlock.$inject = ['$rootScope'];

	function runBlock($rootScope) {
		'use strict';

		console.log('AngularJS run() function...');
	}


})();
